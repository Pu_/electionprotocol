import pytesseract
import cv2
import os
import imutils
from PIL import Image
from pyzbar.pyzbar import decode
from string import digits

import TableDetector
import QRCodeMatcher


def image_resize(image_to_resize, width=None, height=None, inter=cv2.INTER_AREA):
    (h1, w1) = image_to_resize.shape[:2]
    if height is None:
        return image_to_resize
    if width is None:
        r = height / float(h1)
        dim = (int(w1 * r), height)
    else:
        r = width / float(w1)
        dim = (width, int(h1 * r))
    resized = cv2.resize(image_to_resize, dim, interpolation=inter)
    return resized


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return round(x), round(y)


# def filter_dots(old_dots):
#     new_dots = []
#     new_dots.append(old_dots[0])
#     old_dots = sorted(old_dots, key=itemgetter(1))
#     for i in range(len(old_dots)):
#         if old_dots[i][1] - new_dots[-1][1] > 1:
#             new_dots.append(old_dots[i])
#
#     return new_dots

# def remove_lines_segments(image_to_cut):
#     edges = cv2.Canny(image_to_cut, 10, 50, apertureSize=3)
#     matplotlib.pyplot.imshow(edges, cmap='gray')
#     matplotlib.pyplot.show()
#     min_line_length = 3000
#     max_line_gap = 20
#     lines = cv2.HoughLines(edges, 1, np.pi/180, 300)
#     lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, min_line_length, max_line_gap)
#     print(lines)
#     return image_to_cut


pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"
input_folder = "data_extended/"
file_list = os.listdir(input_folder)

for image_name in file_list:

    print(image_name)
    qr_code = decode(Image.open(input_folder + image_name))
    image = cv2.imread(input_folder + image_name)
    (h, w) = image.shape[:2]
    if w > h:
        image = imutils.rotate_bound(image, 90)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # gray = gray[100:-100,100:-100]
    # gray = cv2.blur(gray, (1, 1))
    # print(pytesseract.image_to_string(
    #   gray, config='--psm 1 --oem 1', lang='rus'))

    horizontal, vertical = TableDetector.table_detector(input_folder + image_name)
    # print(horizontal)
    # print(vertical)

    dots = []
    for i in range(0, len(horizontal)-1):

        x1, y1 = line_intersection(([horizontal[i][0], horizontal[i][1]],
                                    [horizontal[i][2], horizontal[i][3]]),
                                   ([vertical[0][0], vertical[0][1]],
                                    [vertical[0][2], vertical[0][3]]))
        x2, y2 = line_intersection(([horizontal[i+1][0], horizontal[i+1][1]],
                                    [horizontal[i+1][2], horizontal[i+1][3]]),
                                   ([vertical[1][0], vertical[1][1]],
                                    [vertical[1][2], vertical[1][3]]))

        dots.append([x1, y1, x2, y2])

        # cv2.circle(gray, (x1, y1), 3, (0, 0, 255), -1)
        # cv2.circle(gray, (x2, y2), 3, (0, 0, 255), -1)
        # cv2.putText(gray, 'a'+str(i), (x1-10*i, y1), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 1, cv2.LINE_AA)
        # cv2.putText(gray, 'b'+str(i), (x2+10*i, y2), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255), 1, cv2.LINE_AA)

    # print(dots)
    # cv2.imwrite('with_lines.jpg', gray)
    # img = matplotlib.image.imread('with_lines.jpg')
    # matplotlib.pyplot.imshow(img)
    # matplotlib.pyplot.show()

    for i in range(10, 20):

        cropped_img = gray[dots[i][1]+3:dots[i][3], dots[i][0]+3:dots[i][2]]

        filename = ("cropped_images/"+str(i)+".png").format(os.getpid())
        cv2.imwrite(filename, cropped_img)

        # print("----------------------------------")
        # print(pytesseract.image_to_string(
        #     cropped_img, config='--psm 13 --oem 3 -c tessedit_char_whitelist=' + digits))

    dots = []
    for i in range(0, len(horizontal)-1):

        x1, y1 = line_intersection(([horizontal[i][0], horizontal[i][1]],
                                    [horizontal[i][2], horizontal[i][3]]),
                                   ([vertical[1][0], vertical[1][1]],
                                    [vertical[1][2], vertical[1][3]]))
        x2, y2 = line_intersection(([horizontal[i+1][0], horizontal[i+1][1]],
                                    [horizontal[i+1][2], horizontal[i+1][3]]),
                                   ([vertical[2][0], vertical[2][1]],
                                    [vertical[2][2], vertical[2][3]]))

        dots.append([x1, y1, x2, y2])

    votes_all = []
    for i in range(11, 18):

        cropped_img = gray[dots[i][1]+4:dots[i][3]+3, dots[i][0]+3:dots[i][2]-100]

        filename = ("cropped_images_2/"+str(i)+".png").format(os.getpid())
        cv2.imwrite(filename, cropped_img)

        votes = pytesseract.image_to_string(
            cropped_img, config='--psm 13 --oem 3 -c tessedit_char_whitelist=' + digits, lang='rus')
        print(votes)
        votes_all.append(votes)
    print(votes_all)
    QRCodeMatcher.match_qr_code(str(qr_code), votes_all)
