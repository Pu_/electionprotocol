import math


def sort_line_list(lines):
    # sort lines into horizontal and vertical
    vertical = []
    horizontal = []
    for line in lines:
        # sort out diagonal lines
        if line[0] > 0:
            print('vertical')
            print(line)
            if math.fabs(line[0] - line[2]) < 100:
                vertical.append(line)
        else:
            if math.fabs(line[1] - line[3]) < 100:
                horizontal.append(line)
    vertical.sort()
    horizontal.sort(key=lambda x: x[1])
    return horizontal, vertical


def filter_close_lines(horizontal, vertical):
    horizontal_new = []
    vertical_new = []
    horizontal_new.append(horizontal[0])
    for line in horizontal:
        if (line[1]+line[3])/2 - (horizontal_new[-1][1] + horizontal_new[-1][3])/2 > 12:
            horizontal_new.append(line)

    vertical_new.append(vertical[0])
    for line in vertical:
        if line[2] - vertical_new[-1][2] > 2:
            vertical_new.append(line)
    return horizontal_new, vertical_new


def table_detector(image):
    import cv2
    import numpy as np
    # import matplotlib.pyplot
    # import matplotlib.image
    print(image[14:-4])
    img = cv2.imread(image)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.threshold(gray, 0, 255,
                         cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    edges = cv2.Canny(gray, 10, 50, apertureSize=3)

    # matplotlib.pyplot.imshow(edges, cmap='gray')
    # matplotlib.pyplot.show()
    # minLineLength = 3000
    # maxLineGap = 20
    lines = cv2.HoughLines(edges, 1, np.pi/180, 270)
    # lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, minLineLength, maxLineGap)
    lines_coordinates = []
    for i in lines:
        a = np.cos(i[0][1])
        b = np.sin(i[0][1])
        x0 = a*i[0][0]
        y0 = b*i[0][0]
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*a)
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*a)

        # cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)
        lines_coordinates.append([x1, y1, x2, y2])
    print(lines_coordinates)
    horizontal, vertical = sort_line_list(lines_coordinates)
    horizontal, vertical = filter_close_lines(horizontal, vertical)

    for i in range(len(horizontal)):
        cv2.line(img, (horizontal[i][0], horizontal[i][1]), (horizontal[i][2], horizontal[i][3]), (0, 0, 255), 2)
    cv2.line(img, (vertical[0][0], vertical[0][1]), (vertical[0][2], vertical[0][3]), (0, 255, 0), 2)
    if len(vertical) > 2:
        cv2.line(img, (vertical[1][0], vertical[1][1]), (vertical[1][2], vertical[1][3]), (255, 0, 0), 2)

    cv2.imwrite('images_with_lines/' + image[14:-4] + '.jpg', img)
    # img = matplotlib.image.imread('with_lines.jpg')
    # matplotlib.pyplot.imshow(img)
    # matplotlib.pyplot.show()

    return horizontal, vertical
