import re
from num2text import triad


def match_qr_code(qr_code, input_rasp):

    pattern = re.compile('/(\d+?):(\d*)')
    # pattern2 = re.compile('-([^-]+?)-')

    def levenshtein_distance(s1, s2):
        if len(s1) > len(s2):
            s1, s2 = s2, s1

        distances = range(len(s1) + 1)
        for i2, c2 in enumerate(s2):
            distances_ = [i2+1]
            for i1, c1 in enumerate(s1):
                if c1 == c2:
                    distances_.append(distances[i1])
                else:
                    distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
            distances = distances_
        return distances[-1]

    def qr_out(qr_code_edit):
        b = dict((int(b[0]), int(b[1])) for b in pattern.findall(qr_code_edit))
        return b

    def get_cand(dic):
        return [value for key2, value in dic.items() if key2 > 50]

    a = qr_out(qr_code)
    # print (transf (input_rasp, len(get_cand(a)) ))
    cand = get_cand(a)
    print(get_cand(a))
    znach_dic = {}
    for elem in cand:
        res = triad(elem, mass=['', '', '', '', ''], sort='jw').replace(' ', ' ').strip()
        res = res.replace('  ', ' ')
        print(elem, res)
        znach_dic[res] = elem

    print(input_rasp)
    for elem in input_rasp:
        rast_min = 999999999
        match = ''
        for key in znach_dic:
            rast = levenshtein_distance(elem, key)
            if rast < rast_min:
                rast_min = rast
                match = key

        print(rast_min, elem, ': ',  match)
